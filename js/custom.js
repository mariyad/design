jQuery(function($) {'use strict';			
					
	// Header
			
	$(window).resize(function(){
		$("header").width($(window).width());
	});					
					
	$(window).scroll(function() {    
		var scroll = $(window).scrollTop();

		if (scroll >= 120) {
			$("header").addClass("stick");
		} else {
			$("header").removeClass("stick");
		}
	});		
				
					
	// Menu 
					
	$(document).ready(function() {
		 
		$('#nav_list').click(function() {
			/*$(this).toggleClass('active');*/
            $('#toggle-icon').toggleClass("open");
			$('.pushmenu-push').toggleClass('pushmenu-push-toright');
			$('.pushmenu-left').toggleClass('pushmenu-open');
		});
    
	});							
			
	
	// Hide Menu
					
	$(document).ready(function () {
		$("#hide_menu").click(function () {
			$(".hide-menu").fadeOut(function () {
				$(".hide-menu").text(($(".hide-menu").text() == 'HIDE') ? 'SHOW' : 'HIDE').fadeIn();
			});
			$("#hide_menu").toggleClass('togglemenu');
			$('header').toggleClass('d-none');
		});
	});	
					
	// File upload Name
					
	$("input[type='file']").on('change',function(){
		var fileName = $(this).val().split('\\').pop();
		$(this).next('.custom-file-label').html(fileName);
	});				
	
	// Custom popover
					
	$(document).ready(function(){
	  $('.products-family-popover').popover({ 
		html : true,
		content: function() {
		  return $('#products_family').html();
		}
	  }).on('shown.bs.popover', function() {});
	});
	
	$(document).ready(function(){
	  $('.brands-popover').popover({ 
		html : true,
		content: function() {
		  return $('#brands').html();
		}
	  }).on('shown.bs.popover', function() {});
	});
	
	$(document).on("click", ".apply-button" , function(){
        $(this).parents(".popover").popover('hide');
    });		
					
					
	// Product Popup Slider jquery
	$("#product_view").on("shown.bs.modal", function() {
		
		$('.slider-for').slick({
		   slidesToShow: 1,
		   slidesToScroll: 1,
		   arrows: false,
		   fade: true,
		   asNavFor: '.small-view-slide',
			  autoplay: false
		 });

		 $('.slider-nav').slick({
		   slidesToShow: 10,
		   slidesToScroll: 10,
		   asNavFor: '.large-view-slide',
		   dots: false,
		   centerMode: false,
		   focusOnSelect: true
		 });

		$('.prev').click(function(){
			$(".slider-for").slick('slickPrev');
		});	

		$('.next').click(function(){
			$(".slider-for").slick('slickNext');
		});				
		
		// select Custom Placeholder
		
		$('.select-two').blur();				
		
		$('.palceholder-select').show(); 
		
		$('.select-two').change(function(){
			if($('.select-two').val() == 'parcel') {
				$('.palceholder-select').show(); 
			} else {
				$('.palceholder-select').hide(); 
			} 
		});
				
	});
					
	// Input and Textarea custom placeholder
		
	$('.palceholder').keypress(function() {
		$(this).siblings('input').focus();
	});

	$('.input-two, .textarea-two').keypress(function() {
		$(this).siblings('.palceholder').hide();
	});

	$('.input-two, .textarea-two').blur(function() {
		var $this = $(this);
		if ($this.val().length == 0)
		$(this).siblings('.palceholder').show();
	});

	$('.input-two, .textarea-two').blur();
					
					
	// Expand and Collapse script
					
	$(document).ready(function() {
		// Configure/customize these variables.
		var showChar = 300;  // How many characters are shown by default
		var ellipsestext = "...";
		var moretext = "Expand";
		var lesstext = "Collapse";


		$('.product-desc').each(function() {
			var content = $(this).html();

			if(content.length > showChar) {

				var c = content.substr(0, showChar);
				var h = content.substr(showChar, content.length - showChar);

				var html = c + '<span class="more-dots">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="#" class="morelink">' + moretext + '</a></span>';
				$(this).html(html);
			}

		});

		$(".morelink").click(function(){
			if($(this).hasClass("less")) {
				$(this).removeClass("less");
				$(this).html(moretext);
			} else {
				$(this).addClass("less");
				$(this).html(lesstext);
			}
			$(this).parent().prev().toggle();
			$(this).prev().toggle();
			return false;
		});
	});
					
	// Product Popup form filter 
					
	$(document).on('change', '.subject', function () {

		let selectValue = $(this).val();
		let content;

		if (selectValue == 'ai') {
			content = '<div class="row"><div class="col-12"> <div class="form-group"> <div class="palceholder palceholder-two"> <label>Message</label> </div><textarea class="form-control textarea-two"></textarea> </div></div><div class="col-12"> <button type="submit" class="blue-button float-right">SEND</button></div></div>';

		} else if (selectValue == 'pq') {
			content = '<div class="row"><div class="col-md-6"> <div class="form-group"> <div class="palceholder palceholder-two"> <label>Quantity</label> </div><input class="form-control input-two"> </div></div><div class="col-md-6"> <div class="form-group"> <div class="palceholder palceholder-two"> <label>Surface</label> </div><input class="form-control input-two"> </div></div><div class="col-12"> <div class="form-group"> <div class="palceholder palceholder-two"> <label>Message</label> </div><textarea class="form-control textarea-two"></textarea> </div></div><div class="col-12"> <button type="submit" class="blue-button float-right">SEND</button> </div></div>';
		} else if (selectValue == 'sd') {
			content = '<div class="row"><div class="col-12"> <div class="form-group"> <div class="palceholder palceholder-two"> <label>Message</label> </div><textarea class="form-control textarea-two"></textarea> </div></div><div class="col-12"> <button type="submit" class="blue-button float-right">SEND</button></div></div>';
		}

		$('#append_subject').html(content);

		$('.palceholder').keypress(function() {
			$(this).siblings('input').focus();
		});

		$('.input-two, .textarea-two').keypress(function() {
			$(this).siblings('.palceholder').hide();
		});

		$('.input-two, .textarea-two').blur(function() {
			var $this = $(this);
			if ($this.val().length == 0)
			$(this).siblings('.palceholder').show();
		});

		$('.input-two, .textarea-two').blur();

	});		
			
					
	// Image Gallery Popup slider
					
	$("#image_gallery").on("shown.bs.modal", function() {
		
		$('.slider-for').slick({
		   slidesToShow: 1,
		   slidesToScroll: 1,
		   arrows: false,
		   fade: true,
		   asNavFor: '.small-view-slide',
			  autoplay: false
		 });

		 $('.slider-nav').slick({
		   slidesToShow: 10,
		   slidesToScroll: 10,
		   asNavFor: '.large-view-slide',
		   dots: false,
		   centerMode: false,
		   focusOnSelect: true
		 });
				
	});
					
	// File Upload
					
	 $(document).ready(function() {
		$('input[type="file"]').imageuploadify();
	});
			
					
	// project Page Cube Show
					
	$('.show_cube').on("click", function () {
		$(this).next('.image-wrap').toggle();
		$(this).next().next('.cube-wrap').toggle();
		$(this).closest('.dark-bg').toggleClass('white-bg');
	});
					
	// Testimonials
					
	$('.testimonial-slide').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		dots: true
	 });
					

});